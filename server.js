/*********************************
 //   envirement variables   //
 ******************************** */
if(process.env.NODE_ENV !== 'production'){
    require ('dotenv').config()
}
const { port,
    SESSION_NAME,
    SESSION_KEY} = require('./model/config');
// modules
//const express = require('express');
var express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const ObjectID = require("bson-objectid");
const multer = require('multer');
const url = require('url');
const ejs = require('ejs');
// allow navegation on cookies
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//var path = require('path');
//var fs = require("fs");

// Load the full build.
const _ = require('lodash');


// local files
const mongooseConfigs = require('./model/mongooseConfigs');
const mongoConfigs = require('./model/mongoConfigs');
const UserController = require('./controller/UserController');
const ConversationController = require('./controller/ConversationController');
const AuthorizationController = require('./controller/AuthorizationController');
const ParticipantController = require('./controller/ParticipantController');
const MessagesController = require('./controller/MessagesController');


// allow navegation on DOM elements
var urlencodedParser = bodyParser.urlencoded({extended:false});
var app = express();
// allow navegation on JSON
var jsonParser = bodyParser.json();
var http = require('http').Server(app);
var io = require('socket.io')(http);


const upload = multer({ dest: "uploads/" });


app.use(cookieParser());
app.use(urlencodedParser);
app.set('view engine', 'ejs');
app.use(express.static("public"));
//app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static("assets"));
app.use('/uploads', express.static("uploads"));
app.use(session({
    name: SESSION_NAME,
    secret: SESSION_KEY,
    resave: false,
    saveUninitialized: true,

}));

const wrap = middleware => (socket, next) => middleware(socket.request, {}, next);
io.use(wrap(session({ secret: SESSION_KEY })));


/*******************************************
 //    creating DB connection mongo      //
 *************************************** */
mongoConfigs.connect(function(err){
    if(!err){
        http.listen(port,function(){
            console.log("Express web server listening on port 80");
        });
    }
});
/*******************************************
 //    creating DB connection mongoose    //
 *************************************** */
mongooseConfigs.connect(function(){
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
        console.log('connected mongoose!!!')
    });
});

/*********************************
 //    mongoose                 //
 ******************************** */

/*********************************
 //Set the schema user            //
 ******************************** */
var userSchema = new mongoose.Schema({
    user: String,
    pass: String,
    photo: String,
    email: Date,
    friends: [],
});

/*********************************
 //Set the behaviour (apply rule here)//
 ******************************** */

userSchema.methods.verifyPass = function (pass) {
    return pass===this.pass;
}

/*********************************
 //Compile the schema into a model//
 ******************************** */
var user = mongoose.model('user', userSchema);

/***********************************
 //Set the schema conversation    //
 ******************************** */

var conversationSchema = new mongoose.Schema({
    name: String,
    usersList: [],
    creator: String,
    terminated:Boolean,
});

/*********************************
 //Set the behaviour (apply rule here)//
 ******************************** */
/*
conversationSchema.methods.verifyPass = function (pass) {
    return pass===this.pass;
}*/

/*********************************
 //Compile the schema into a model//
 ******************************** */
var conversation = mongoose.model('conversation', conversationSchema);


/***********************************
 //Set the schema conversation history    //
 ******************************** */

var conversationHistSchema = new mongoose.Schema({
    conversation_id:mongoose.ObjectId,
    name: String,

});

/*********************************
 //Set the behaviour (apply rule here)//
 ******************************** */
/*
conversationHistSchema.methods.verifyPass = function (pass) {
    return pass===this.pass;
}*/

/*********************************
 //Compile the schema into a model//
 ******************************** */
var conversationHist = mongoose.model('conversationHist', conversationHistSchema);


/*********************************
 //Set the schema participant      //
 ******************************** */
var participantSchema = new mongoose.Schema({
    conversation_id:mongoose.ObjectId,
    user: String,
    userRole: String,
    startDate: Date,
    endDate: Date,
});

//Set the behaviour (apply rule here)
participantSchema.methods.verifyRole = function (userId) {
    if(userId==this.userId)
        return userRole;
}

//Compile the schema into a model
var participant = mongoose.model('participant', participantSchema);



/*********************************
 //Set the schema authorization   //
 ******************************** */
var authorizationSchema = new mongoose.Schema({
    conversation_id:mongoose.ObjectId,
    user: String,
    type: String,
    state: String

});

//Set the behaviour (apply rule here)
authorizationSchema.methods.verifyRole = function (userId) {
    if(userId==this.userId)
        return userRole;
}

//Compile the schema into a model
var authorization = mongoose.model('authorization', authorizationSchema);

/*********************************
 //Set the schema message   //
 ******************************** */
var messageSchema = new mongoose.Schema({
    message: String,
    date: {
        type:Date,
        default: Date.now
    },
    sender_id: {
        type: Schema.Types.ObjectId,
        ref:'user'
    },
    conversation_id:{
        type: Schema.Types.ObjectId,
        ref:'conversation'
    },
    reply_id: {
        type: Schema.Types.ObjectId,
        ref: 'message'
    },
    type: String,


});

//Set the behaviour (apply rule here)
messageSchema.methods.verifyRole = function (userId) {
    if(userId==this.userId)
        return userRole;
}

//Compile the schema into a model
var Message = mongoose.model('message', messageSchema);


 /*********************************
 //   MiddleWare Authentication   //
 ******************************** */

const redirectLogin = (req, res, next)=>{
    if(!req.session.userId){
        res.redirect('/')
    }
    else {
        next()
    }
}
const redirectConversations = (req, res, next)=>{
    if(req.session.userId){
        res.redirect('/Conversations')
    }
    else {
        next()
    }
}

/*********************************
 //   routes                     //
 ******************************** */

app.get('/', function (req, res) {
    const {userId} = req.session
    console.log(process.env.URLMONGODB)
    res.render('root');
});

app.get('/login',redirectConversations, function (req, res) {
    //req.session.userId = 12
    console.log('login get session\n',req.session)


    /*************************************************
     * testing mongodb id's
     ************************************************/

    x=ObjectID("608efebeaaf9914054f0dc8a").toString()
    y=ObjectID("608efebeaaf9914054f0dc8a").toString()
    console.log('chave mongodb',x)
    if(y===x) {console.log('\n equal x\n',x,'\n',y)} else console.log('\n not equal')
    x=ObjectID("608efebeaaf9914054f0dc8a")
    y=ObjectID("608efebeaaf9914054f0dc8a")
    console.log('chave mongodb',x)
    if(y.equals(x)) {console.log('\n equal x\n',x,'\n',y)} else console.log('\n not equal')
    /*
    UserController.getAllUsers(function(result){
        console.log('id do utilizador',result)
        console.log('id do utilizador',result[0]._id)
        var c = result[0]._id;
        var  b = '608efebeaaf9914054f0dc8a';
        if(c===b) {console.log('\n equal x\n',c,'\n',b)} else console.log('\n not equal')
    },'123');*/
    /*************************************************
     * end of tests
     ************************************************/



    res.render('./login/login');
});

/*********************************
 //   login auth routes  done      //
 ******************************** */
app.post('/login',jsonParser,  function (req, res) {
//ajax verify login
    //res.cookie('user', '');
    //res.cookie('pass', '');

    console.log(jsonParser)
    var user = req.body.user
    var pass = req.body.pass
    //console.log(user)
    //console.log( req.body)

    UserController.authenticationUser(function (result,empty){
        //console.log(result)
        console.log('empty ',empty)
        var badAuth = {};

        if (empty == true) {
            badAuth.user = "notExists";
            res.status(404).send({data:badAuth});
        }
        if(result.length != 0){
            if(user!=result[0].user) badAuth.user = "error"
            if(pass!=result[0].pass) badAuth.pass = "error"
            if(user==result[0].user && pass==result[0].pass ){
                req.session.userId = user;
                console.log('login post session\n',req.session)
                //res.cookie('pass', pass);
                res.status(200).send({data:'OK'});
            }
            else{
                res.status(404).send({data:badAuth});
                //response ajax
            }
        }

    },user)

});

/*********************************
 //   register routes  done        //
 ******************************** */
app.get('/register',redirectConversations, function (req, res) {
    res.render('./users/register');
});

app.post('/register',upload.single('photo'),function(req,res){

    //check if theres a user with same name
    //console.log(req.body.user);
    //compare user name with value on db dont allow to repeat user name
    UserController.getDuplicate(function(result){
        var present=false;
        for(var i=0; i<result.length && present===false; ++i){
            //test returns message if equal
            (req.body.user==result[i].user)?present=true:present=false;
        }
        //console.log(present);
        if(!present){

            UserController.addUser(req,function(err,result){
                //console.log(result)
                if(!err)
                    res.redirect('/usersList');
                else
                    //
                    console.log("Erro!");
            });

        }
        else
            res.redirect('usersList')
    });
});

/************************************
//   List Users in DB              //
// should only be access by admin  //
/********************************* */

app.get('/usersList',function(req,res){
    UserController.getAllUsers(function(result){
        res.render('./users/usersList', {data:result});
    })
});


app.post('/user', function(req,res){
    console.log(req.body.user);
    console.log(req.body.pass);
	console.log(req.body.email);
	//console.log(req.body.photo);

    res.redirect('/user/'+req.body.user+'/'+req.body.pass+'/'+req.body.email);
});


/******************************************
//   Conversations routes  working here  //
**************************************** */
app.get('/conversations',redirectLogin,jsonParser,  function (req, res) {
    //console.log('conversations get session\n',req.session)
    var user = req.session.userId
    //console.log('\n',user)
    UserController.getAllUsers( function(result){
        ConversationController.getConversations( req,function (conversationsList){
            AuthorizationController.getAuthorization( req, function (authenticationList){
                ParticipantController.getParticipants(user,function(participantsList){
                    res.render('./conversations/conversations', {data:result,conversationsList:conversationsList,authenticationList:authenticationList,user:user,participant:participantsList});
                })
            })
        })
    })
});

app.get('/conversations/:id', function (req, res) {
    res.render('./conversations/conversations');
});

app.get('/addConversation',redirectLogin, function (req, res) {
    //console.log('parameters',req.params.id)
    var user = req.session.userId
    //console.log('user', user)
    UserController.getAllUsers(function(result){
        res.render('./conversations/newConversation', {data:result,user:user});
    })

});

/*********************************
 //   addNewConversation     //
 ******************************** */
app.post('/addNewConversation',jsonParser,  function (req, res) {
    var name = req.body.name
    var userList = req.body.usersList
    userList.push(req.session.userId)
    //console.log(name)
    //console.log( req.body)

    ConversationController.insertConversation(req,function (res){

        //console.log(res)

    })
    console.log("conversation added")
    res.status(200).send({data:'OK'});
});

/*********************************
 //   editConversation                   //
 ******************************** */

app.post('/editConversation',jsonParser,  function (req, res) {
    console.log('post editConversation get user',req.session.userId)
    let nameConversation = req.body.nameConversation
    console.log('post name conversation',nameConversation)
    //putting conversation name on session variable
    const name = req.session
    req.session.name = nameConversation;
    console.log('post session name conversation',req.session)
    res.status(200).send({nameConversation:nameConversation});


});

app.get('/editConversation',jsonParser,  function (req, res) {
    let user = req.session.userId
    //console.log('get editConversation user',req.session.userId)
    let nameConversation = req.session.name
    console.log('get name conversation',nameConversation)
    //console.log('name conversation',req)
    nameConversation=[{name:nameConversation}];
    conversation.find({name:req.session.name},'usersList name _id',function (err, conversation) {
        if (err) return console.log(err);

        req.session.usersList = conversation[0].usersList
        req.session._id = conversation[0]._id
        //console.log('convUser',conversation[0].usersList)
        //console.log('usersList',req.session)
        UserController.getAllUsers(function(result){
            res.render('./conversations/editConversation', {nameConversation:nameConversation,data:result,usersConv:conversation,user:user,_id:conversation});
        })
    })
});



app.post('/updateConversation',jsonParser,  function (req, res) {
    //console.log(jsonParser)
    var name = req.session.name
    var usersList = req.session.usersList
    var newName = req.body.name
    var newUsersList = req.body.usersList

    // console.log('Update',newUsersList)

    newUsersList.push(req.session.userId)
    //console.log('update',name)
    //console.log('update', req.body)


    /***************************
    // Compare arrays users
    *******************************/
    // lost authorization
    var difBef = _.difference(usersList, newUsersList)
    console.log("compare user - new",_.difference(usersList, newUsersList));
    console.log('User add authentication',typeof (difNew))


    // add new authorizations
    var difNew=_.difference(newUsersList, usersList)
    console.log("compare new - user ",difNew);
    console.log('User add authentication',typeof (difBef))

    //add conversation history
    if(name!=newName){
        instance = new conversationHist ({
            conversation_id: ObjectID(req.body.id),
            name: name,

        });
        instance.save(function (err, instance) {
            if (err) return console.error(err);
        })
    }



    conversation.updateOne({name:name}, { usersList: newUsersList, name: newName },function (err, docs) {
        if (err){
            console.log(err)
        }
        else{
            req.session.name = newName
            req.session.usersList =  newUsersList
            // console.log("Updated Docs : ", docs);
        }
    });

    //  lost authorization
    authorization.find({conversation_id:ObjectID(req.body.id), user: { "$in" : difBef}},'_id state type conversation_id',function (err, authorization) {
        if (err) return console.log(err);
        //console.log('authorization lose',authorization)

        })

    authorization.updateMany({conversation_id:ObjectID(req.body.id), user: { "$in" : difBef}}, {  state: 'inactive' },function (err, docs) {
        if (err){
            console.log(err)
        }
        else{
            // console.log("Updated Docs : ", docs);
        }
    });

    participant.updateMany({conversation_id:ObjectID(req.body.id), user: { "$in" : difBef}, endDate:''}, {  endDate: parseFloat(Date.now()) },function (err, docs) {
        if (err){
            console.log(err)
        }
        else{
            // console.log("Updated Docs : ", docs);
        }
    });

    // add autorizathion
    // difNew reference to array
    //does the user have authentication on conversation ?

    console.log('type of users',difNew.length)
    if(difNew.length != 0){
        console.log('passou aqui' )
        difNew.forEach(function users (element,index,array){
            authorization.findOne( {conversation_id:ObjectID(req.body.id), user:element}, function (err, result) {
                if (err) return console.log(err);
                if (!result) {
                    instance = new authorization ({
                        conversation_id: ObjectID(req.body.id),
                        user: String(element),
                        type: 'invite',
                        state: ''
                    });
                    instance.save(function (err, instance) {
                        if (err) return console.error(err);
                    })
                }
                else{
                    authorization.updateOne({conversation_id:ObjectID(req.body.id), user: String(element),  type:{ $ne: 'creator' } },{state:''},function (err, docs) {
                        if (err){
                            console.log(err)
                        }
                        else{
                            //console.log("Updated Docs : ", docs);
                        }
                    });
                }
            })
        })
    }



    res.status(200).send({data:'OK'});


});

/*********************************
 //   participant               //
 ******************************** */

app.post('/addParticipant',jsonParser,(req, res)=>{
    //add participant
    instance = new participant ({
        conversation_id: ObjectID(req.body.conversation_id),
        user: req.body.user,
        role: 'guest',
        startDate: Date.now(),
        endDate: String('')
    });
    instance.save(function (err, instance) {
        if (err) return console.error(err);
    })

    //give authorization

    authorization.updateOne({conversation_id:ObjectID(req.body.conversation_id), user:req.body.user},{ state:"active"  }, function (err, docs) {
        if (err){
            console.log(err)
        }
        else{
            console.log("Updated Docs authorization : ", docs);
        }
    });

    console.log('add participant ');
    res.status(200).send({data:'OK'});
})

app.post('/inactiveAuthorization',jsonParser, async (req, res)=>{
    //inactive authorization

    authorization.updateOne({conversation_id:ObjectID(req.body.conversation_id), user:req.body.user},{ state:"inactive"  }, function (err, docs) {
        if (err){
            console.log(err)
        }
        else{
            //console.log("Updated Docs authorization : ", docs);
        }
    });
    console.log('conversation Id',ObjectID(req.body.conversation_id) );
    conversation.findOne({_id:ObjectID(req.body.conversation_id)},'usersList',function(err, docs){
        console.log("users List conversation",docs.usersList)
        const array = docs.usersList;
        console.log(array);
        const index = array.indexOf(req.body.user);
        if (index > -1) {
            array.splice(index, 1);
        }
        console.log('update usersList conversation inside',array);
         conversation.updateOne({_id:ObjectID(req.body.conversation_id)},{ usersList: array  }, function (err, docs) {
            if (err){
                console.log(err)
            }
            else{
                //console.log("Updated usersList conversations : ", docs);
            }
        });
    })
    console.log('authorization inactive ');
    res.status(200).send({data:'OK'});
})




/*********************************
 //   message routes      //
 ******************************** */
let conversation_id
app.get('/messages',jsonParser,function (req,res){
     conversation_id = req.query.conversation_id;
    let user = req.query.user;
    //console.log('request query',conversation_id);
    //console.log('request query',user);
    UserController.getAllUsers( function(result) {
        Message.find({conversation_id: ObjectID(conversation_id)}, function (messages) {
            console.log('\nmessages docs', JSON.parse(messages));
            res.render('./messages/messages', {messages: JSON.parse(messages), data: result, user: user, conversation_id:conversation_id});
        });
    });

});


io.on('connection', function(socket){
    console.log(`new connection ${socket.id}`);
    socket.on('join_room', function(room){
        console.log('room',room)
        socket.join(room);
    });
    socket.on('send message',function (msg,conversation_id,sender_id,path){
        //console.log('conversation_id',conversation_id);
        MessagesController.addMessage(msg,conversation_id,sender_id,function(err,result){
            if(err) {
                console.log(err);
            }
            else {
                //console.log('addMessage result',result);
                let message_id = result.insertedId;
                //console.log('message _id',message_id);
                console.log('Update');
                io.in(conversation_id).emit('update',msg, path,message_id)
                //io.sockets.emit('update',msg, path,message_id);
            }
        });
    });
});



/*
app.post('/messages', function(req,res){

    //just for checking
    var message = req.body.message;
    console.log("This is the message: "+message);


    MessagesController.addMessage(req, function(err,result){
        if(err)
        {
            console.log("Error!");
        }
    })
});
*/




/*********************************
 //   drop collecttions          //
 ******************************** */
app.get('/admin',(req, res)=>{
    res.render('./admin/admin')
})

app.post('/admin',jsonParser,(req, res)=>{
    var collectionDB = req.body.collectionDB;
    mongoose.connection.db.dropCollection(collectionDB, function(err, result) {
    });
    console.log('DB colection droped ',collectionDB);
    res.status(200).send({data:'OK'});
})

/*********************************
 //   logout                   //
 ******************************** */
app.get('/logout', (req, res)=>{
    req.session.destroy(err=>{
        if(err){
            return res.redirect('/conversations')
        }
        //res.clearCookie(cookie, {path:'/'})
        res.redirect('/login')
    })

})


/*********************************
 //   not used                   //
 ******************************** */

//just a route that returns users as json back to the client
app.get('/users/raw',function(req,res){
    UserController.getAllUsers(function(result){
        res.json(result);
    })
});




app.get('/user',function(req,res){
    var queryObject = url.parse(req.url,true).query;

    // Notice that besides the callback there are more arguments. If those arguments are not present in the querystring, undefined will be passed
    // Arguments are optional in javascript (contrasting with Java) so function getAllNotes from NotesController is reused
    UserController.getAllUsers(function(result){
        res.render('./users/usersList', {data:result});
    }, queryObject.user, queryObject.pass, queryObject.email, queryObject.photo)
});




/*********************************
 //   mongoose                   //
 ******************************** */
/*********************************
 //  test save  mongoose          //
 ******************************** */
/*
 instance = new participant ({
    conversationId: 'test',
    userId: 'test',
    userRole: 'test',
    startDate: '2021-05-04',
     });
instance.save(function (err, instance) {
    if (err) return console.error(err);
});
*/

/*
let instance = new conversation ({
    name: '1620311903944',
    usersList: ['luis','fernando','rui'],
    creator: 'admin',
    terminated:false,
});
instance.save( function (err, instance) {
    if (err) return console.error(err);
});

*/


/*********************************
 //  test find  mongoose          //
 ******************************** */
/*
conversation.find({},'usersList',function (err, conversation) {
    if (err) return console.log(err);
    var conv = session.conv
    conv = conversation[0]
    console.log(conv);
})
*/
