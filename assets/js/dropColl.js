
function dropConversation(){
    $.ajax({
        url: "admin",
        type: "POST",
        data: JSON.stringify({
            collectionDB: 'conversations'
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (){
            $("#message").html("droped conversations")
            location.href = `/admin`
        },
        error:function (response){
            alert("fail to drop conversations");
        }
    });
}
function dropAutorization(){
    $.ajax({
        url: "admin",
        type: "POST",
        data: JSON.stringify({
            collectionDB: 'authorizations'
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (){
            $("#message").html("droped authorizations")
            location.href = `/admin`
        },
        error:function (response){
            alert("fail to update authorizations");
        }
    });
}
function dropParticipant(){
    $.ajax({
        url: "admin",
        type: "POST",
        data: JSON.stringify({
            collectionDB: 'participants'
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (){
            $("#message").html("droped participations")
            location.href = `/admin`;
        },
        error:function (response){
            alert("fail to drop participations");
        }
    });
}
function dropMessages(){
    $.ajax({
        url: "admin",
        type: "POST",
        data: JSON.stringify({
            collectionDB: 'messages'
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (){
            $("#message").html("droped messages")
            location.href = `/admin`;
        },
        error:function (response){
            alert("fail to drop messages");
        }
    });
}