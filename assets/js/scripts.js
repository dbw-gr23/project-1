function registerValidation(event){

    var validate = false;
    //var userValue =  document.getElementById("user").value;
    var userValue =  $('input[name="user"]').val();
    var passValue =  $('input[name="pass"]').val();
    var emailValue =  $('input[name="email"]').val();
    var photoValue =  $('input[name="photo"]').val();

    if(userValue==''){
        //document.getElementById("userValidation").innerHTML="user field empty";
        $("#userValidation").html("user field empty");
        $("#userValidation").css("background-color","red");
        validate = true;
    }else $("#userValidation").html("");

    if(passValue==''){
        $("#passValidation").html("pass field empty");
        $("#passValidation").css("background-color","red");
        validate = true;
    }
    else $("#passValidation").html("");

    if(emailValue=='') {
        $("#emailValidation").html("email field empty");
        $("#emailValidation").css("background-color","red");
        validate = true;
    }
    else $("#emailValidation").html("");

    if(photoValue=='') {
        $("#photoValidation").html("email field empty");
        $("#photoValidation").css({"background-color":"red"});
        validate = true;
    }
    else {
        $("#photoValidation").html("");
    }
    //verify if users exists missing ajax



    if (validate == true){
        event.preventDefault();
    }

}

function handleLogin(event){

    event.preventDefault();

    var validate = false;
    var userValue =  $('input[name="user"]').val();
    var passValue =  $('input[name="pass"]').val();

    if(userValue==''){
        //document.getElementById("userValidation").innerHTML="user field empty";
        $("#userValidation").html("user field empty");
        $("#userValidation").css("background-color","red");
        validate = true;
    }else $("#userValidation").html("");

    if(passValue==''){
        $("#passValidation").html("pass field empty");
        $("#passValidation").css("background-color","red");
        validate = true;
    }
    else $("#passValidation").html("");

    //verify on db credencials send request to server
    if(!validate){
        $.ajax({
            url: "login",
            type: "POST",
            data: JSON.stringify({
                user: userValue,
                pass: passValue
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (){
                location.href = `/conversations`},
            error:function (response){
                var message = '';

                response.status;
                message = response.responseText;


                if (message.includes('user')){
                    message = "User does not exist in DB";
                    $("#errorLogin").css("background-color","red");
                }else if(message.includes('pass')){
                    message = "Password does not match User";
                    $("#errorLogin").css("background-color","red");
                }else{
                    message = "Unknow error contact Admin";
                    $("#errorLogin").css("background-color","red");
                }

                $("#errorLogin").html(message)

            }
        });
    }



    /*
    $.post("/login",{user:userValue,pass:passValue},function (){
        success:location.href = "/conversation"
    })
*/
};

function addConversationValidation(event){
    event.preventDefault();

    $("#userValidation").html("");
    var validate = false;
    var nameValue =  $('input[name="name"]').val();
    var userValue =  $("#usersList option:selected").text()
    var userArray=[];
    $("#usersList option:selected").each(function() {
        userArray.push($(this).val());
    });
    //console.log('selected',userValue)

    // conversation name can be blank passes date now
    /*
    if(nameValue==''){
        $("#nameValidation").html("conversation name field empty");
        $("#nameValidation").css("background-color","red");
        validate = true;
    }
    else $("#nameValidation").html("");
    */
    if(nameValue==''){
        nameValue = Date.now();
    }

    if(userValue==''){
        //document.getElementById("userValidation").innerHTML="user field empty";
        $("#userValidation").html("no selection");
        $("#userValidation").css("background-color","red");
        validate = true;
    }else {
        for(var i=0; i<userArray.length; ++i){
            $("#userValidation").append('<li class="alignElement">'+userArray[i]+'</li>');
        }
    }

    if(!validate){
        $.ajax({
            url: "addNewConversation",
            type: "POST",
            data: JSON.stringify({
                name: nameValue,
                usersList: userArray
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (){location.href = `/conversations`},
            error:function (response){
                alert("fail to add conversation");
            }
        });
    }


};

function editConversation(event,data){
    event.preventDefault()

    //alert('event handler data passed '+ data)
    $.ajax({
            type: "post",
            url: "editConversation",
            data: JSON.stringify({
                nameConversation: data

            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                location.href = `/editConversation`
                //alert('ok conversation name '+ data);
            },
            error: function(response) {
                alert('error');
            }
        });
    //});
}


function editConversationValidation(event){
    event.preventDefault();

    $("#userValidation").html("");
    var validate = false;
    var nameValue =  $('input[name="name"]').val();
    var userValue =  $("#usersList option:selected").text()
    var userArray=[];
    $("#usersList option:selected").each(function() {
        userArray.push($(this).val());
    });
    //console.log('selected',userValue)

    // conversation name can be blank passes date now
    /*
    if(nameValue==''){
        $("#nameValidation").html("conversation name field empty");
        $("#nameValidation").css("background-color","red");
        validate = true;
    }
    else $("#nameValidation").html("");
    */
    if(nameValue==''){
        nameValue = Date.now();
    }

    if(userValue==''){
        //document.getElementById("userValidation").innerHTML="user field empty";
        $("#userValidation").html("no selection");
        $("#userValidation").css("background-color","red");
        validate = true;
    }else {
        for(var i=0; i<userArray.length; ++i){
            $("#userValidation").append('<li class="alignElement">'+userArray[i]+'</li>');
        }
    }

    if(!validate){
        $.ajax({
            url: "addNewConversation",
            type: "POST",
            data: JSON.stringify({
                name: nameValue,
                usersList: userArray
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (){location.href = `/conversations`},
            error:function (response){
                //$("#errorLogin").html(response.responseJson.data);
                alert("fail to add conversation");
            }
        });
    }


};
/**********************
 Update Conversation
 **********************/
function updateConversationValidation(event){
    event.preventDefault();

    $("#userValidation").html("");
    $("#nameValidation").html("");
    var validate = false;
    var nameValue =  $('input[name="name"]').val();
    var id =  $('#id').text();
    var userValue =  $("#usersList option:selected").text()
    var userArray=[];
    $("#usersList option:selected").each(function() {
        userArray.push($(this).val());
    });

    if(nameValue==''){
        $("#nameValidation").html("conversation name field empty");
        $("#nameValidation").css("background-color","red");
        validate = true;
    }

    if(userValue==''){
        //document.getElementById("userValidation").innerHTML="user field empty";
        $("#userValidation").html("no selection");
        $("#userValidation").css("background-color","red");
        validate = true;
    }else {
        for(var i=0; i<userArray.length; ++i){
            $("#userValidation").append('<li class="alignElement">'+userArray[i]+'</li>');
        }
    }

    if(!validate){
        $.ajax({
            url: "updateConversation",
            type: "POST",
            data: JSON.stringify({
                name: nameValue,
                usersList: userArray,
                id:id
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (){location.href = `/conversations`},
            error:function (response){
                alert("fail to update conversation");
            }
        });
    }
}



function addParticipant(event,user,conversation_id){
    event.preventDefault();
    //alert('event handler data passed '+ conversation_id)
    $.ajax({
        url: "addParticipant",
        type: "POST",
        data: JSON.stringify({
            conversation_id: conversation_id,
            user: user,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (){location.href = `/conversations`},
        error:function (response){
            alert("fail to add participant");
        }
    });
}



function inactiveAuthorization(event,user,conversation_id){
    event.preventDefault();
    //alert('event handler data passed '+ conversation_id)
    $.ajax({
        url: "inactiveAuthorization",
        type: "POST",
        data: JSON.stringify({
            conversation_id: conversation_id,
            user: user,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (){location.href = `/conversations`},
        error:function (response){
            alert("fail to decline invite");
        }
    });
}


function goMessages(event,conversation_id,user){
    event.preventDefault();
    let url = `/messages?conversation_id=`+conversation_id+'&user='+user;
    location.href = url;

}
function requestComeBack(event,conversation_id){

}
