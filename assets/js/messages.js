$(document).ready(function(){
    var socket = io();

    $('#sendButton').click(function(){

        var msg = $("#messageField").val();
        console.log(msg);
        if (msg != "") {
            socket.emit("send message", msg);
        }else{
            alert("Please insert a valid message!!!!");
        }
    });

    socket.on('update',function(msg){
        $('#chatBox').append('<br>' + '<div id="circle">' + '<textarea id="textArea" readonly>' + (msg) + '</textarea>' + '</div>' + '<br><br>');
        $('#chatBox').append('')
        $('#messageField').val("");
    });
});