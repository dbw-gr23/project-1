const Participation = require('../model/Participant');

function getParticipants(user,callback){
    Participation.getParticipants(user,callback)
}


function editParticipant(){
    Participation.editParticipant()
}

module.exports = {
    getParticipants,
    editParticipant
}