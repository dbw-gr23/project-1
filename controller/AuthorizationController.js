const Authorization = require('../model/Authorization');

function getAuthorization(req, callback){
    Authorization.getAuthorization(req.session.userId, callback)
}
function editAuthorization(){
    Authorization.editAuthorization()
}

module.exports = {
    getAuthorization,
    editAuthorization
};