const Message = require('../model/Message');
const ObjectID = require("bson-objectid");

function addMessage(msg,conversation_id,sender_id, callback) {
    let date = new Date();
    let conversation = ObjectID(conversation_id);
    Message.insertMessage(msg,date,sender_id,conversation,-1,callback);
}

module.exports = {
    addMessage
}