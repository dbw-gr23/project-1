const User = require('../model/User');

function addUser(req,callback){
    User.insertUser(req.body.user,req.body.pass,req.body.email,req.file.path ,callback);
}

function getAllUsers(callback, user, pass, email,photo){
    User.getAllUsers(callback, user, pass, email,photo);
}

function getDuplicate(callback, user){
    User.getDuplicate(callback,user);
    }

function authenticationUser(callback, user){
    User.authenticationUser(callback,user);


}

module.exports = {
    addUser,
    getAllUsers,
    getDuplicate,
    authenticationUser
};