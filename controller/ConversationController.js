const Conversations = require('../model/Conversation');

async function insertConversation(req,callback){
   await Conversations.insertConversation(req.body.name,req.session.userId,req.body.usersList,callback)
}

function updateUserConversation(id,user,callback){
   Conversations.updateUserConversation(id,user,callback)

}

function getConversations(req,callback){
    Conversations.getConversations(req.session.userId,callback)

}
function getConversation(user,callback){
    Conversations.getConversation(user,callback)

}






module.exports = {
    insertConversation,
    updateUserConversation,
    getConversations,
    getConversation

};