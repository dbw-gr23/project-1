const mongoose = require('mongoose');
const { URLMONGODB} = require('./config');
module.exports = {

    connect: function (callback) {
        //connection string Group 23
        mongoose.connect(URLMONGODB, {useNewUrlParser: true, useUnifiedTopology: true});
    },
    mongoose
};