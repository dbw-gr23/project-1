var mongoConfigs = require('./mongoConfigs');

function insertUser(user,pass,email,photo,callback){
    var db = mongoConfigs.getDB();
    db.collection('users').insertOne({user:user,pass:pass,email:email,photo:photo},function(err,result){
           callback(err,result);
    });
}

function getAllUsers(callback, user, pass, email,photo){
    var db = mongoConfigs.getDB();

    var filters = { };

    if(user !== undefined) filters.user = user;
    if(pass !== undefined) filters.pass = pass;
    if(email !== undefined) filters.email = email;
    if(photo !== undefined) filters.photo = photo;

    db.collection('users').find(filters).toArray(function(err,result){
        callback(result);
    });
}

function getDuplicate(callback, user){
    var db = mongoConfigs.getDB();

    var filters = { };

    if(user !== undefined) filters.user = user;
    db.collection('users').find(filters).toArray(function(err,result){
        callback(result);
    });
}

function authenticationUser(callback, user){
    var db = mongoConfigs.getDB();

    var filters = { };
    var empty = false;

    if(user !== undefined) filters.user = user;
    db.collection('users').find(filters).toArray(function(err,result){
        console.log('resultdb')
        if(result.length == 0) {
            console.log('resultdb', result)
            empty=true;
            console.log('empty ',empty)
        }
        callback(result,empty);
    });
}

module.exports = {
    insertUser,
    getAllUsers,
    getDuplicate,
    authenticationUser

};
