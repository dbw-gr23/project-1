var mongoConfigs = require('./mongoConfigs');


function insertMessage(message,date,senderId,conversationId,replyId,callback){
    var db = mongoConfigs.getDB();
    db.collection('messages').insertOne({message:message,date:date,senderId:senderId,conversationId:conversationId,replyId:replyId}, function (err,result){
        callback(err,result);
    });
}

module.exports={
    insertMessage
};
/*
const mongoose = require('./mongooseConfigs');
const Schema = mongoose.Schema;
*/
/*********************************
 //Set the schema message   //
 ******************************** */
/*
var messageSchema = new mongoose.Schema({
    message: String,
    date: {
        type:Date,
        default: Date.now
    },
    sender_id: {
        type: Schema.Types.ObjectId,
        ref:'user'
    },
    conversation_id:{
        type: Schema.Types.ObjectId,
        ref:'conversation'
    },
    reply_id: {
        type: Schema.Types.ObjectId,
        ref: 'message'
    },
    type: String,


});
*/
//Set the behaviour (apply rule here)
/*
messageSchema.methods.verifyRole = function (userId) {
    if(userId==this.userId)
        return userRole;
}

//Compile the schema into a model
var Message = mongoose.model('message', messageSchema);

exports.listMesssages = (cb)=>{
    Message.find({},{_id:0,message:1,date:1,sender_id:1,conversation_id:1,reply_id:1,type:1})
        .populate({path:'sender_id',model: User})
        .populate({path:'conversation_id',model: Conversation})
        .populate({path:'reply_id',model: Message})
        .exec()
        .then(doc=>(cb(doc)))
        .catch(err => cb(null, err));

};
*/