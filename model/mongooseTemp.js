/*********************************
 //    mongoose                 //
 ******************************** */
const mongoose = require('mongoose');


/*********************************
 //    user                      //
 ******************************** */
//Set the schema user
var userSchema = new mongoose.Schema({
    user: String,
    pass: String,
    photo: String,
    email: Date,
    friends: [],
});

//Set the behaviour (apply rule here)
userSchema.methods.verifyPass = function (pass) {
    return pass===this.pass;
}

//Compile the schema into a model
var user = mongoose.model('user', userSchema);


/*********************************
 //    conversation               //
 ******************************** */

//Set the schema conversation
var conversationSchema = new mongoose.Schema({
    name: String,
    usersList: [],
    creator: String,
    terminated:Boolean,
});

//Set the behaviour (apply rule here)
/*
conversationSchema.methods.verifyPass = function (pass) {
    return pass===this.pass;
}*/

//Compile the schema into a model
var conversation = mongoose.model('conversation', conversationSchema);


let instance = new conversation ({
    name: '1620311903944',
    usersList: [{name:'luis',name:'fernando',name:'rui'}],
    creator: 'admin',
    terminated:false,
});
instance.save(function (err, instance) {
    if (err) return console.error(err);
});


conversation.find({},'creator',function (err, conversation) {
    if (err) return handleError(err);
    console.log(conversation);
})


/*********************************
 //    participant               //
 ******************************** */

//Set the schema participant
var participantSchema = new mongoose.Schema({
    conversationId: String,
    userId: String,
    userRole: String,
    startDate: Date,
    endDate: Date,
});

//Set the behaviour (apply rule here)
participantSchema.methods.verifyRole = function (userId) {
    if(userId==this.userId)
        return userRole;
}

//Compile the schema into a model
var participant = mongoose.model('participant', participantSchema);