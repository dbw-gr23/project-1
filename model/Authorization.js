var mongoConfigs = require('./mongoConfigs');

function getAuthorization(user,callback){
        var db = mongoConfigs.getDB();

        var filters = { };

        if(user !== undefined) filters.user = user;
        db.collection('authorizations').find(filters).toArray(function(err,result){

            callback(result);
        });
}
function editAuthorization(){

}

module.exports = {
    getAuthorization,
    editAuthorization
};