const dotenv = require('dotenv');
dotenv.config();
module.exports = {
    NOD_ENV:process.env.NOD_ENV,
    port: process.env.PORT,
    URLMONGODB:process.env.URLMONGODB,
    SESSION_NAME: process.env.SESSION_NAME,
    SESSION_KEY: process.env.SESSION_KEY
};