var mongoConfigs = require('./mongoConfigs');


function insertConversation(name,creator,usersList,callback){
    var db = mongoConfigs.getDB();
    var nameString = String(name);
    db.collection('conversations').insertOne({name:nameString,usersList:usersList,creator:creator,terminated:false},function(err,resultConversation){
        //console.log(err)

        for(var i=0;i < usersList.length;++i){
            //console.log('users for',usersList[i])
            //console.log('lenght',usersList.length)
            //console.log('value i on for',i)
            if(usersList[i]!=creator){
                //console.log('value i on if',i)
                db.collection('authorizations').insertOne({conversation_id:resultConversation.insertedId,user:usersList[i],type:'invite',state:''},function(err,resultAuthorization){
                    callback(err,resultAuthorization);
                });
            }
            else{
                db.collection('authorizations').insertOne({conversation_id:resultConversation.insertedId,user:creator,type:'creator',state:'active'},function(err,resultAuthorization){
                    //console.log('value i on else',i)
                    //console.log('user creator',usersList[i])
                    db.collection('participants').insertOne({conversation_id:resultConversation.insertedId,user:creator,role:'admin',startDate: Date.now(),endDate:null},function(err,resultParticipant){
                        callback(err,resultParticipant);
                    });
                    callback(err,resultAuthorization);
                });
            }
        };

        //console.log('inserted id teste',resultConversation.insertedId)
        callback(err,resultConversation);
    });
}

function updateUserConversation(id,user,callback){
    var db = mongoConfigs.getDB();

    var filters = { };
    var empty = false;

    if(user !== undefined) filters.user = {user:user};
    filters._id = id;

    db.collection('conversations').find(filters).toArray(function(err,result){
        if(result.length == 0) {
            console.log('resultdb', result)
            empty=true;
            console.log('empty ',empty)
        }
        callback(result,empty);
    });

}

function getConversations(user,callback){
    var db = mongoConfigs.getDB();

    var filters = { };

    if(user !== undefined) filters.usersList = user;
    db.collection('conversations').find(filters).toArray(function(err,result){
        callback(result);
    });

}
function getConversation(name,callback){


}



module.exports = {
    insertConversation,
    updateUserConversation,
    getConversations,
    getConversation

};